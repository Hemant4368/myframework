#
#  Be sure to run `pod spec lint MyFramework.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see https://guides.cocoapods.org/syntax/podspec.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |spec|


  spec.name         = "MyFramework"
  spec.version      = "1.0.0"
  spec.summary      = "it is demo purpose CustomFrameWork."
  spec.description  = <<-DESC
	A Custom frame work control like the demo, but in a function form.The Custom frame work control is a completely customizable widget that can be used in any iOS app. It also plays a little victory fanfare.
                   DESC

  spec.homepage     = "https://github.com/Hemant4368/CustomFrameWork"
 
  spec.license      = "MIT"

  spec.author       = { "Hemant Patel" => "hemant.optimumbrew@gmail.com" }
  
  spec.platform     = :ios, "12.0"


  spec.source       = { :git => "https://Hemant4368@bitbucket.org/Hemant4368/myframework.git" }

  spec.source_files  = "MyFramework"

  spec.swift_version = "5.0"
 
 spec.dependency 'Alamofire'

end
